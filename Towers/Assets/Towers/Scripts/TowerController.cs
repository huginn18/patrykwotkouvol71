﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class TowerController : MonoBehaviour {
	#region PublicFields
	#endregion
	#region PrivateFields
	[SerializeField]
	private float _roationDelay = 0.5f;
	[SerializeField]
	private float _roationPeroid = 12;
	#endregion

	#region UnityMethods
	private void Awake () { }
	private void Start () {
		
	}
	private void Update () { }
	private void OnEnable(){
		if(GameMaster.Instance.first == false){
			StartCoroutine(WaitForActivation());
		} else{
			GameMaster.Instance.first = false;
			this.GetComponent<SpriteRenderer>().color = Color.red;
			StartCoroutine (StartShooting ());
		}
	}
	#endregion

	#region PublicMethods
	public void ActivateFinalRound(){
		StopCoroutine(WaitForActivation());
		StopCoroutine(StartShooting());

		StartCoroutine(StartShooting());
	}
	#endregion
	#region PrivateMethods
	private IEnumerator WaitForActivation(){
		yield return new WaitForSecondsRealtime(6.0f);
		this.GetComponent<SpriteRenderer>().color = Color.red;
		StartCoroutine(StartShooting());
	}
	private IEnumerator StartShooting () {
		int moves = 0;
		while (moves != _roationPeroid) {
			yield return new WaitForSecondsRealtime (0.5f);
			float rotation = Random.Range (15, 46) + this.transform.eulerAngles.z;
			this.transform.rotation = Quaternion.Euler (0, 0, rotation);
			
			FireProjectile(); 

			moves++;
		}

		this.GetComponent<SpriteRenderer>().color = Color.white;
	}
	private void FireProjectile(){
		GameObject bullet = GameMaster.Instance.bulletPool.GetElement();

		bullet.transform.position = this.transform.position;
		bullet.transform.rotation = this.transform.rotation;

		bullet.GetComponent<BulletController>().owner = this.gameObject;
	}
	#endregion
}