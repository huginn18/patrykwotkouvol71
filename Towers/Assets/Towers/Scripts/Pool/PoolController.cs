﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolController : MonoBehaviour {
	#region PublicFields
	public int ActiveElementsCount {
		get {
			return activeElements.Count;
		}
	}

	public List<GameObject> activeElements {get; protected set;}
	#endregion
	#region PrivateFields

	[SerializeField]
	private GameObject _prefab;

	[SerializeField]
	private int _SpawnAmount = 8;

	private List<GameObject> _inActiveElements;

	private bool _hasComponent;
	#endregion

	#region UnityMethods
	private void Awake () {
		activeElements = new List<GameObject> ();
		_inActiveElements = new List<GameObject> ();

		if (_prefab.GetComponent<PoolElement> () != null)
			_hasComponent = true;

		SpawnNewPoolElements ();
	}
	private void Start () { }
	private void Update () { }
	#endregion

	#region PublicMethods
	public GameObject GetElement () {
		if (_inActiveElements.Count == 0)
			SpawnNewPoolElements ();

		GameObject element = _inActiveElements[0];
		_inActiveElements.Remove (element);
		activeElements.Add (element);

		element.SetActive (true);
		element.transform.SetParent (null);

		return element;
	}
	public void ReturnToPool (GameObject element) {
		activeElements.Remove (element);
		_inActiveElements.Add (element);

		element.SetActive (false);
		element.transform.SetParent (this.transform);
	}
	#endregion
	#region PrivateMethods
	private void SpawnNewPoolElements () {
		for (int i = 0; i < _SpawnAmount; i++) {
			GameObject newElement = Instantiate<GameObject> (_prefab);
			if (_hasComponent == false)
				newElement.AddComponent<PoolElement> ();
			newElement.GetComponent<PoolElement> ().pool = this;

			newElement.SetActive (false);
			newElement.transform.SetParent (this.transform);

			_inActiveElements.Add (newElement);
		}
	}

	#endregion
}