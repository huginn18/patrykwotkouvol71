﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PoolElement : MonoBehaviour {
	#region PublicFields
	public PoolController pool;
	#endregion
	#region PrivateFields
	#endregion

	#region UnityMethods
	private void Awake(){
	}
	private void Start(){
	}
	private void Update(){
	}
	#endregion

	#region PublicMethods
	public void ReturnToPool(){
		this.pool.ReturnToPool(this.gameObject);
	}
	#endregion
	#region PrivateMethods
	#endregion
}
