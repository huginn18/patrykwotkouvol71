﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BulletController : MonoBehaviour {
	#region PublicFields
	public GameObject owner;
	#endregion
	#region PrivateFields

	[SerializeField]
	private float _lifeSpan;
	private float _speed = 4;
	#endregion

	#region UnityMethods
	private void Awake () { }
	private void Start () { }
	private void Update () {
		Move ();
	}
	private void OnEnable () {
		_lifeSpan = Random.Range (1, 5);
		StartCoroutine (MonitorLifeSpan ());
	}

	private void OnTriggerEnter2D (Collider2D collider) {

		if (collider.tag == "Tower" && collider.gameObject != owner) {
			StopCoroutine (MonitorLifeSpan ());
			GetComponent<PoolElement> ().ReturnToPool ();
			collider.gameObject.GetComponent<PoolElement> ().ReturnToPool ();
		}
	}
	#endregion

	#region PublicMethods
	#endregion
	#region PrivateMethods
	private void Move () {
		this.transform.Translate (this.transform.up * _speed * Time.deltaTime);
	}

	private IEnumerator MonitorLifeSpan () {
		yield return new WaitForSecondsRealtime (_lifeSpan);
		if (GameMaster.Instance.finalRound == false) {
			GameObject tower = GameMaster.Instance.towerPool.GetElement ();
			tower.transform.position = this.transform.position;
		}

		this.GetComponent<PoolElement> ().ReturnToPool ();
	}
	#endregion
}