﻿/*  === ===== === = === ===== === = === ===== === = === ===== ===
			Abandon all hope — Ye Who Enter Here

	   Copyright (c) Patryk Huginn Woźnicki <huginn@huginn.me>
	=== ===== === = === ===== === = === ===== === = === ===== === */

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameMaster : MonoBehaviour {
	#region PublicFields
	public static GameMaster Instance;

	public PoolController bulletPool;
	public PoolController towerPool;

	public bool first = true;
	public bool finalRound;
	#endregion
	#region PrivateFields
	[SerializeField]
	private Text _towersCount;
	#endregion

	#region UnityMethods
	private void Awake(){
		if(Instance != null){
			this.gameObject.SetActive(false);
			Destroy(this.gameObject);
			return;
		}
		Instance = this;
	}
	private void Start(){
		GameObject tower = towerPool.GetElement();
	}
	private void Update(){
		_towersCount.text = "Towers: " +towerPool.ActiveElementsCount;
		if(towerPool.ActiveElementsCount >=100){
			finalRound = true;
			foreach(GameObject go in towerPool.activeElements){
				go.GetComponent<TowerController>().ActivateFinalRound();
			}
		}
	}
	#endregion

	#region PublicMethods
	#endregion
	#region PrivateMethods
	#endregion
}
